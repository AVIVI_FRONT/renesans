function showMenu() {
    $(document).on('click', '.js-show-menu', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.header__links').removeClass('active');
        } else {
            $(this).addClass('active');
            $('.header__links').addClass('active');
        }
    });
}

function scrollTo() {
    $(document).on('click', 'a.scrollto', function () {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top-140;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        $('.js-show-menu').removeClass('active');
        $('.header__links').removeClass('active');
        return false;
    });
}
function goSliders() {
    $('.js-slide-docs').slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 1150,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 905,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.js-slide-rounds').slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 905,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

function windowScroll() {
    $(window).on('scroll', function () {

       var x = $('.header').offset();
       var height = (x.top);
        if(height > 100) {
            $('.header__box').addClass('fix_head');
        } else {
            $('.header__box').removeClass('fix_head');
        }
    });
}

$(document).ready(function(){
    windowScroll();
    goSliders();
    scrollTo();
    showMenu();
});